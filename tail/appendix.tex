%!TEX root = ../phd.tex
\chapter{Appendix}
\label{sec:appendix}

\section{\yy Translation Correctness}
\label{sec:yy-translation-correctness}

% Introducing HM.
In this section we formalize the core translation of \yy in rank-1 polymorphic lambda-calculus with the
  $\klet$ construct for introducing universally quantified terms. To the core calculus we add
  primitive types $\kbool$ and $\kint$.
  The calculus does not include type reconstruction: all type annotations are explicitly stated.
  The syntax of our core language is the following:
\newcommand{\alt}{\mid}
\begin{align*}
\shortintertext{Terms:}
t       &::= t\;t \alt x[\tau]\dotsm[\tau] \alt \llet{x:
  \sigma}{t}{t} \alt v\\
v       &::= \func{x: \tau}{t} \alt c\\
c       &::= \ktrue \alt \kfalse \alt 0 \alt 1 \alt \dots\\
\shortintertext{Types:}
\sigma  &::= \tau \alt \univ{X}{\sigma}\\
\tau    &::= X \alt \Func{\tau}{\tau} \alt \btyp\\
\btyp   &::= \kbool \alt \kint
\end{align*}

Since the evaluation rules for this calculus are well known we state only the typing rules in \figref{types}.

\newcommand{\type}[1]{T-\textsc{#1}}
\newcommand{\tra}[1]{$\ttrone{\_}$-\textsc{#1}}
\newcommand{\ttr}[1]{$\langle\_\rangle$-#1}
\newcommand{\tto}[1]{\langle #1 \rangle}
\newcommand{\tta}[1]{\langle #1 \rangle_{arg}}


\boxfig{types}{The Typing Rules.}{


\begin{multicols}{2}[\judgement{Type Rules}{}]

\infax[\type{Bool}]{\tctx{\Gamma}{\ktrue, \kfalse: \kbool}}

\infax[\type{Int}]{\tctx{\Gamma}{0, 1, \dots : \kint}}

\end{multicols}

\begin{multicols}{2}

\infrule[\type{Abs}]
  {\tctx{\Gamma, x: \tau_1}{t: \tau_2}}
  {\tctx{\Gamma}{\func{x: \tau_1}{t}: \Func{\tau_1}{\tau_2}}}

\infrule[\type{App}]
  {\tctx{\Gamma} t_1: \Func{\tau_1}{\tau_2} \;\;\; \tctx{\Gamma} t_2: \tau_1}
  {\tctx{\Gamma}{t_1 \; t_2 : \tau_2}}

\end{multicols}


\infrule[\type{TApp}]
  { x: \univ{X_1}{\dotsm\univ{X_n}{\tau}} \in \Gamma }
  {\tctx{\Gamma}{x[\tau_1]\dotsm[\tau_n] : \subst{X_1}{\tau_1}\dotsm\subst{X_n}{\tau_n} \tau}}


\infrule[\type{Let}]
  { \tctx{\Gamma} t_1: \sigma \;\;\; \tctx{\Gamma, x: \sigma} t_2: \tau  }
  {\tctx{\Gamma}{\llet{x: \sigma}{t_1}{t_2}: \tau}}


\begin{multicols}{2}
\end{multicols}


}

To simulate the deep embedding, and at the same time keep the calculus of the meta-language simple, we introduce additional constructs in the object language. We assume that the object language supports type-application at the type level for the deep embedding type \code{R}. The \code{R} can be viewed as a built-in higher-kind type (similar to \code{List} or \code{Array}). For all $\tau$ types $\Rep{\tau_1} = \Rep{\tau_2}$ if and only if $\tau_1 = \tau_2$. The type substitution for \code{R} types behaves as expected. Terms of type \code{R} are instantiated with the \code{lift} function that is always in context in the object language. The \code{lift} function has the following type in context:
\begin{lstparagraph}
  $\tctx{\Gamma}{lift:\univ{X}{\Func{X}{\Rep{X}}}}$
\end{lstparagraph}

\boxfig{transformation}{\yy Translation.}{

\begin{multicols}{2}[\judgement{Term Translation ($\ttrone{\_}$)}{}]

\infyyax{\tra{Abs}}
  {\func{x: \tau}{t}}{\func{x: \tto{\tau}}{\ttrone{t}}}

\infyyax{\tra{App}}
  {t_1\;t_2}{\ttrone{t_1}\;\ttrone{t_2}}

\end{multicols}

\infyy{\tra{Const}}
  {\tctx{\Gamma}{c: \btyp}}
  {c}{\mathtt{lift}[\btyp] \; c}

\infyyax{\tra{TApp}}
  {x[\tau_1]\dotsm[\tau_n]}{x[\tta{\tau_1}]\dotsm[\tta{\tau_n}]}


\infyyax{\tra{Let}}
  {\llet{x: \sigma}{t_1}{t_2}}{\llet{x: \tto{\sigma}}{\trone{t_1}}{\ttrone{t_2}}}


\begin{multicols}{2}[\judgement{Type Translation ($\tto{\_}$ and $\tta{\_}$)}{}]

\inftax{\ttr{Base}}
  {\tto{\btyp}}{\Rep{\btyp}}

\inftax{\ttr{TVar}}
  {\tto{X}}{\Rep{X}}

\end{multicols}

\begin{multicols}{2}

\inftax{\ttr{Func}}
  {\tto{\Func{\tau_1}{\tau_2}}}{\Func{\tto{\tau_1}}{\tto{\tau_2}}}


\inftax{\ttr{Abs}}
  {\tto{\univ{X} \sigma}}{\univ{X} \tto{\sigma} }

\end{multicols}

\begin{multicols}{2}

\inftax{\ttr{FArg}}
  {\tta{\Func{\tau_1}{\tau_2}}}{error}

\infrule[\ttr{Arg}]
  {\tau \neq \Func{\tau_1}{\tau_2}}
  {\tta{\tau} = \tau}
\end{multicols}
}


\newtheorem*{lemma}{Lemma}
\newtheorem*{theorem}{Theorem}
\newcommand{\case}[2]{\emph{Case} #1 {\bf:} #2}

For types, we define the equivalence relation ($=$) structurally:
\begin{multicols}{3}

\infax[]{\kbool = \kbool}

\infax[]{\kint = \kint}

\infax[]{X = X}

\end{multicols}

\begin{multicols}{2}

\infrule[]
  {\tau_1 = \tau_1' \;\;\; \tau_2 = \tau_2'}
  {\Func{\tau_1}{\tau_2} = \Func{\tau_1'}{\tau_2'}}

\infrule[]
  {\tau = \tau'}
  {\univ{X}{\tau} = \univ{X}{\tau'}}

\end{multicols}
The described equivalence relation requires type variables to have the same name for the types to be equivalent. This, ``stricter'' version of the equivalence relation is chosen as it makes proving the following theorem simpler.

\begin{theorem}
If a term $\tctx{\Gamma} t: \sigma$ is well typed and the translation is successful, the translated term's type is equivalent to the type translation of the original term's type $\tctx{\Gamma} \ttrone{t}: \tto{\tau}$.
\end{theorem}

\begin{proof}
We conduct the proof by using induction on the term translation ($\trone{\_}$):

\case{\tra{Const}}{For translation of constants there are two cases in the typing derivation:\begin{itemize}
\item \case{\type{Bool}}{Based on the premise the type of a translated term is $\kbool$. Given the signature of $lift$, the translated term has a type $\Rep{\kbool}$. This is equivalent to the type translation of the original term's type (\ttr{Base}).}
\item \case{\type{Int}}{Analogous to \type{Bool}.}
\end{itemize}}

\case{\tra{Abs}}{By applying the typing derivation \type{Abs} on the translated term and the induction hypothesis on the abstraction body, the translated term's type is $\Func{\tto{\tau_1}}{\tto{\tau_2}}$. This is equivalent to the type translation of the original term's type (\ttr{Func}).}

\case{\tra{App}}{By applying the induction hypothesis on both sub-terms of the application and applying the typing rule \type{App}, the type of the translated term is $\tto{\tau_2}$. This is equivalent to the type translation of the original term's type.}

 \case{\tra{TApp}}{There are two cases to consider for this translation rule:}
  \begin{itemize}

   \item When one of the type arguments is a function type the translation rule \ttr{FArg} is triggered and the type translation is not successful. The theorem holds as the translated term was not well formed.

   \item By applying the typing derivation on the translated term and comparing it to the type translation of the original term the following needs to hold:
     $$\tto{\subst{X_1}{\tau_1}\dotsm\subst{X_n}{\tau_n} \tau} = \subst{X_1}{\tta{\tau_1}}\dotsm\subst{X_n}{\tta{\tau_n}} \tto{\tau}$$

    This equality holds by induction on the type structure:

    \case{Base type}{Trivially holds.}

    \case{Type variable}{Holds as the substituted type must be either a base type or a type variable. For both cases it trivially holds by following translation rules \ttr{Base} and \ttr{TVar}, and type substitution.}

    \case{Function type}{Holds by the induction hypothesis, substitution, and type translation rules for functions.}
   \end{itemize}

\case{\tra{Let}}{By the induction hypothesis on sub-terms in the translation and the \type{Let} rule.}

\end{proof}



\section{Hardware and Software Platform for Benchmarks}
\label{sec:hardware-platform}

In this thesis all benchmarks are executed on the same platform: \begin{itemize}

\item {\bf Hardware configuration} is the following: the \emph{CPU} is Intel Core i7-2600K with 4 physical cores each having 2 virtual-cores and has 3.4 GHz working frequency, the \emph{main memory} is DDR3 with 1333 MHz working frequency. We assure that hyper-threading and frequency-scaling are disabled during executions.

\item {\bf Software configuration} is the following: for compilation of Scala programs we use Scala 2.11.7, the programs execute on the HotSpot 64-Bit Server (24.51-b03) virtual machine.

\end{itemize}

{\bf Measurements} are performed in the same way for all benchmarks. Each benchmark is executed until the virtual machine is warmed up and stabilized. During the measurements we assure that no garbage collection happens. Finally, all reported numbers in the these are a mean of the last 10 measurements. In all experiments the variance is smaller than 5\%.

